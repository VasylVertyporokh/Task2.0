//
//  UserEventProtocol.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 17.06.2022.
//

import Foundation

protocol UserEventProtocol: MockModelProtocol {
    var infoMassage: String { get set }
    var startDate: String { get set }
    var startTime: String { get set }
    var endDate: String { get set }
    var endTime: String { get set }
    var expectedDistance: Int { get set }
}
