//
//  UserActivityProtocol.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 17.06.2022.
//

import Foundation
protocol UserActivityProtocol: MockModelProtocol {
    var userName: String { get set }
    var distance: Int { get set }
    var averageSpeed: Int { get set }
    var activityTime: String { get set }
    var countOfSteps: Int { get set }
}
