//
//  MockModel.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 15.06.2022.
//

import Foundation

struct UserActivity: UserActivityProtocol {
    var userName: String
    var distance: Int
    var averageSpeed: Int
    var activityTime: String
    var countOfSteps: Int
}
