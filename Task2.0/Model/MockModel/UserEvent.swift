//
//  UserEvent.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 16.06.2022.
//

import Foundation

struct UserEvent: UserEventProtocol {
    var infoMassage: String
    var startDate: String
    var startTime: String
    var endDate: String
    var endTime: String
    var expectedDistance: Int
}
