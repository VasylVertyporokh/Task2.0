//
//  DetailModel.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 16.06.2022.
//

import Foundation

struct DetailModel {
    let titel: String
    let playImageName: String
    let primaryButtonTitle: String
    let homeImageName: String
    let listImageName: String
    let tabStartItemName: String
    let tabTrainigsItemName: String
}
