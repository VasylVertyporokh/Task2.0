//
//  DetailViewModel.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 16.06.2022.
//

import Foundation

final class DetailViewModel {
    let detailModel = DetailModel(
        titel: "Katia",
        playImageName: "playIcon",
        primaryButtonTitle: "Train at the event",
        homeImageName: "homeIcon",
        listImageName: "listIcon",
        tabStartItemName: "Start",
        tabTrainigsItemName: "Trainigs"
    )
    
    let mockModel: [[MockModelProtocol]] = [
        [
            UserActivity(userName: "Katia",
                         distance: 12,
                         averageSpeed: 10,
                         activityTime: "00:16:00",
                         countOfSteps: 14100)
        ],
        [
            UserEvent(infoMassage: "We are running for little Katia to help her fight cance",
                      startDate: "17.08.2022",
                      startTime: "09:00",
                      endDate: "17.08.2022",
                      endTime: "12:00",
                      expectedDistance: 15)
        ]
    ]
}
