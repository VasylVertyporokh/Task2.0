//
//  UIView+Extansion.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 14.06.2022.
//

import Foundation
import UIKit

extension UIView {
    
    func setOrangeShadow() {
        self.layer.shadowRadius = 10
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor.orange.cgColor
        self.layer.shadowOffset = .zero
    }
    
    func setGrayShadow() {
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.4
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = .zero
    }
}
