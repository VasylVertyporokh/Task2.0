//
//  DetailViewControllerExtansion.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 17.06.2022.
//

import Foundation
import UIKit

// MARK: - UITableViewDelegate
extension DetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true)
    }
}

// MARK: - UITableViewDataSource
extension DetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.mockModel.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.mockModel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = viewModel.mockModel[indexPath.section]
        
        if let userActivityObject = object[indexPath.row] as? UserActivity {
            guard let userActivityCell = tableView.dequeueReusableCell(withIdentifier: "AmountOfTrainingCell", for: indexPath) as? AmountOfTrainingCell else {
                return UITableViewCell()
            }
            userActivityCell.setUserActivity(activity: userActivityObject)
            return userActivityCell
            
        } else if let userEventObject = object[indexPath.row] as? UserEvent {
            guard let amountOfTrainigCell = tableView.dequeueReusableCell(withIdentifier: "ActualEventCell", for: indexPath) as? ActualEventCell else {
                return UITableViewCell()
            }
            amountOfTrainigCell.setActualEvent(event: userEventObject)
            return amountOfTrainigCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let possibleSection = EventsSections.allCases[section]
        
        switch possibleSection {
        case .trainings:
            return "The amount of training"
        case .events:
            return "Current events"
        }
    }
}
