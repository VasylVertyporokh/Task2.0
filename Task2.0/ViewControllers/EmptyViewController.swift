//
//  EmptyViewController.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 14.06.2022.
//

import Foundation
import UIKit

final class EmptyViewController: UIViewController  {
    // MARK: - Outlets
    @IBOutlet private var placeholderImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    // MARK: - Private methods
    private func configureUI() {
        placeholderImageView.image = UIImage(named: "boo")
    }
}
