//
//  DetailViewController.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 14.06.2022.
//

import Foundation
import UIKit

enum EventsSections: Int, CaseIterable {  // незнаю чи цей енам має бути в цьому місці, чи краще його винести в окремий файл
    case trainings = 0
    case events = 1
}

// екстеншин переніс в окремий файл але є до тебе питання можливо здзвонимось задам
final class DetailViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var primaryButton: UIButton!
    @IBOutlet private var userNameLabel: UILabel!
    
    var viewModel: DetailViewModel! // тут зробив форсом бачив в прикладі, можливо має бути по іншому
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureTableView()
        configureTabBar()
    }
    
    // MARK: - Private methods
    private func configureUI() {
        userNameLabel.text = viewModel.detailModel.titel
        primaryButton.tintColor = .white
        primaryButton.backgroundColor = .orange
        primaryButton.layer.cornerRadius = primaryButton.bounds.height / 2
        primaryButton.setImage(UIImage(named: viewModel.detailModel.playImageName), for: .normal)
        primaryButton.setTitle(viewModel.detailModel.primaryButtonTitle, for: .normal)
        primaryButton.setTitleColor(.white, for: .normal)
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ActualEventCell", bundle: nil), forCellReuseIdentifier: "ActualEventCell")
        tableView.register(UINib(nibName: "AmountOfTrainingCell", bundle: nil), forCellReuseIdentifier: "AmountOfTrainingCell")
        tableView.separatorStyle = .none
        
    }
    
    private func configureTabBar() {
        tabBarController?.tabBar.tintColor = UIColor.orange
        tabBarController?.tabBar.backgroundColor = .white
        tabBarController?.tabBar.items?[0].image = UIImage(named: viewModel.detailModel.homeImageName)
        tabBarController?.tabBar.items?[1].image = UIImage(named: viewModel.detailModel.listImageName)
        tabBarController?.tabBar.items?[0].title = viewModel.detailModel.tabStartItemName
        tabBarController?.tabBar.items?[1].title = viewModel.detailModel.tabTrainigsItemName
    }
    
    // MARK: - Actions
    @IBAction func primaryButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}
