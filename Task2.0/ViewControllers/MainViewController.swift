//
//  MainViewController.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 14.06.2022.
//

import UIKit

final class MainViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var goToDetailImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        gesture()
    }
    
    // MARK: - Private methods
    private func configureUI() {
        goToDetailImageView.image = UIImage(named: "run")
        containerView.layer.cornerRadius = containerView.bounds.height / 2
        containerView.setOrangeShadow()
    }
    
    private func gesture() {
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.goToDetails))
        containerView.addGestureRecognizer(gesture)
    }
    
    // MARK: - Action
    @objc private func goToDetails() {
        guard let tabBarVC = storyboard?.instantiateViewController(identifier: "TabBarViewController") else {
            return
        }
        
        tabBarVC.modalTransitionStyle = .flipHorizontal
        tabBarVC.modalPresentationStyle = .currentContext
        
        if let detailVC = tabBarVC.children.first(where: { $0 is DetailViewController }) as? DetailViewController {
            let viewModel = DetailViewModel()
            detailVC.viewModel = viewModel
            show(tabBarVC, sender: self)
        }
    }
}
