//
//  ActualEventCell.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 15.06.2022.
//

import Foundation
import UIKit

// також бачив такий підхід кофігурування вю, незнаю чи це є правельно бо як на мене це зручно коли полів багато
struct ActualEventPlaceholders {
    let infoTitle: String
    let distance: String
    let startDateImageName: String
    let finishDateImageName: String
    let distanceImageName: String
    let importButtonTitle: String
    let goToDetailButtonTitle: String
    let arrow: String
}

final class ActualEventCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet var containerView: UIView!
    
    @IBOutlet private var startPlaceholderLabel: UILabel!
    @IBOutlet private var finishPlaceholderLabel: UILabel!
    @IBOutlet private var distancePlaceholderLabel: UILabel!
    
    @IBOutlet private var infoTitleLabel: UILabel!
    @IBOutlet private var infoSubTitleLabel: UILabel!
    @IBOutlet private var startDateLabel: UILabel!
    @IBOutlet private var finishDateLabel: UILabel!
    @IBOutlet private var startTimeLabel: UILabel!
    @IBOutlet private var finishTimeLabel: UILabel!
    
    @IBOutlet private var distanceKmLabel: UILabel!
    
    @IBOutlet private var startDateImageView: UIImageView!
    @IBOutlet private var finishDateImageView: UIImageView!
    @IBOutlet private var distanceImageView: UIImageView!
    
    @IBOutlet private var importButton: UIButton!
    @IBOutlet private var goToDetailButton: UIButton!
    
    private var placeholder: ActualEventPlaceholders {
        ActualEventPlaceholders(
            infoTitle: "Train as part of the event",
            distance: "Distance",
            startDateImageName: "calendars",
            finishDateImageName: "calendars",
            distanceImageName: "walk",
            importButtonTitle: "Import from Google Fit",
            goToDetailButtonTitle: "Details",
            arrow: "arrow"
        )
    }
    
    override func awakeFromNib() {
        super .awakeFromNib()
        configureCell()
    }
    
    // MARK: - Private methods
    private func configureCell() {
        containerView.layer.cornerRadius = 20
        containerView.setGrayShadow()
        infoTitleLabel.textColor = .orange
        
        startDateImageView.image = UIImage(named: placeholder.startDateImageName)
        finishDateImageView.image = UIImage(named: placeholder.finishDateImageName)
        distanceImageView.image = UIImage(named: placeholder.distanceImageName)
        
        infoTitleLabel.text = placeholder.infoTitle
        distancePlaceholderLabel.text = placeholder.distance
        
        importButton.setTitle(placeholder.importButtonTitle, for: .normal)
        importButton.setTitleColor(.orange, for: .normal)
        goToDetailButton.tintColor = .orange
        goToDetailButton.setTitle(placeholder.goToDetailButtonTitle, for: .normal)
        goToDetailButton.setTitleColor(.orange, for: .normal)
        goToDetailButton.semanticContentAttribute = .forceRightToLeft
        goToDetailButton.setImage(UIImage(named: placeholder.arrow), for: .normal)
        goToDetailButton.imageEdgeInsets.left = 16
        
        selectionStyle = .none
    }
    
    // MARK: - Private methods
    func setActualEvent(event: UserEvent) {
        infoSubTitleLabel.text = event.infoMassage
        startDateLabel.text = event.startDate
        finishDateLabel.text = event.endDate
        startTimeLabel.text = event.startTime
        finishTimeLabel.text = event.endTime
        distanceKmLabel.text = "\(event.expectedDistance)"
    }
    
    // MARK: - Actions
    @IBAction func importButtonAction(_ sender: UIButton) {
        
    }
    
    @IBAction func goToDetailAction(_ sender: UIButton) {
        
    }
}
