//
//  AmountOfTrainingCell.swift
//  Task2.0
//
//  Created by Vasil Vertiporokh on 14.06.2022.
//

import Foundation
import UIKit

final class AmountOfTrainingCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet private var containerView: UIView!
    
    @IBOutlet private var primaryImageView: UIImageView!
    @IBOutlet private var speedImageView: UIImageView!
    @IBOutlet private var timeImageView: UIImageView!
    @IBOutlet private var stepsImageView: UIImageView!
    
    @IBOutlet private var distancePlaceholderLabel: UILabel!
    @IBOutlet private var speedPlaceholderLabel: UILabel!
    @IBOutlet private var timePlaceholderLabel: UILabel!
    @IBOutlet private var stepsPlacehlderLabel: UILabel!
    @IBOutlet private var distanceTraveledLabel: UILabel!
    @IBOutlet private var averageSpeedLabel: UILabel!
    @IBOutlet private var activityTimeLabel: UILabel!
    
    @IBOutlet private var numberOfStepsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }
    
    // MARK: - Private methods
    private func configureCell() {
        selectionStyle = .none
        primaryImageView.image = UIImage(named: "runner")
        containerView.layer.cornerRadius = 20
        distancePlaceholderLabel.text = "Calculated distance"
        speedImageView.image = UIImage(named: "speedometer")
        timeImageView.image = UIImage(named: "clock")
        stepsImageView.image = UIImage(named: "walk")
        containerView.setGrayShadow()
    }
    
    // MARK: - Public methods
    func setUserActivity(activity: UserActivity) {
        distanceTraveledLabel.text = "\(activity.distance)"
        averageSpeedLabel.text = "\(activity.averageSpeed)"
        activityTimeLabel.text = activity.activityTime
        numberOfStepsLabel.text = "\(activity.countOfSteps)"
    }
}
